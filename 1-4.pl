%exo 1.4
homme(adrien).
homme(hugo).
homme(bernard).
homme(alain).
homme(guy).
homme(pierre).
femme(veronique).
parent(adrien, hugo).
parent(hugo,bernard).
parent(hugo,alain).
parent(adrien, guy).
parent(guy, pierre).
parent(guy, veronique).

frat(X,Y) :-
	parent(Z,X),
	parent(Z,Y),
	X\=Y.

frere(X,Y) :-
	homme(X),
	frat(X,Y).

soeur(X,Y) :-
	femme(X),
	frat(X,Y).

neu(X,Y) :-
	parent(Z,X),
	frat(Z,Y).

neuveu(X,Y) :-
	homme(X),
	neu(X,Y).

niece(X,Y) :-
	femme(X),
	neu(X,Y).

oncle(X,Y) :-
	homme(X),
	neu(Y,X).

tante(X,Y) :-
	femme(X),
	neu(Y,X).

cous(X,Y) :-
	parent(Z,X),
	neu(Y,Z).

cousin(X,Y) :-
	homme(X),
	cous(X,Y).

cousine(X,Y) :-
	femme(X),
	cous(X,Y).
