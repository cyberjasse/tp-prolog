%exo 1.1
femme(isabel).
femme(anne).

parent(anne,pierre).
parent(anne,filip).
parent(jean,pierre).
parent(michel,filip).

decede(michel).
decede(isabel).

seuleChargeeDeFamille(F) :-
	femme(F),
	parent(F,E),
	parent(H,E),
	H\=F,
	decede(H).
%Ici, F est seuleChargeeDeFamille si F est une femme et soit E, F est parent de E et soit H, H est parent de E, H est différent de F et H est décédé.
%Parmis les faits qu'on a, parmis les femmes, seule anne est parent d'un enfant (filip) dont un autre, michel, est parent de filip est michel est mort.

%exo 1.3
%X=Y. %devrais afficher X=Y
%X is Y. %devrais afficher false ou alors une erreur si Y n'existe pas, si il n'est pas instancié
%X=Y, Y=Z, Z=1. %devrais afficher X=1 et Y=1 et Z=1 ? et non! affiche X=Y,Y=Z,Z=1
%X=1, Z=Y, X=Y. %affiche X=1, Z=Y, X=Y. ? Même pas ! affiche X=Z, Z=Y, Y=1. Car si je comprends bien, arrivé à X=Y, l'interpreteur sait que y=Z et donc l'interpreteur interprète que X=Z Mais il sait aussi que X=1 donc si X=Y alors Y=1.
%X is X, X is 1+1. %X is X vaut true mais X is 1+1 devrais valoir false et true,false vaut false. On parie ? Et non car j'ai oublié que à coté du is, il faut une instance or X n'est pas instancié et donc ça renvoie une erreur. Et au fait, X is 1+1 retourne X=2
%1+2 == 1+2. %devrait retourner true mais est ce syntaxiquement correct ? Non c'est bon. Mais ça vérifie si les expressions sont égales, pas les valeurs
%X == Y. %Il ne sont pas instancié donc ça devrait retourner une erreur. Non ça retourne faux.
%1 =:= 2-1. %On teste si les valeurs sont égales et donc ça va retourner true
X =:= Y. %Il n'y a pas de valeur à vérifier. donc ça renvoie une erreur.
