% exo 2.1 version recursive 1
fibonacci(0,1) :- !.
fibonacci(1,1) :- !.
fibonacci(N,FIB) :- N > 1, fibonacci(N,FIB,1,1).
fibonacci(1,FIB,R1,_) :-
	FIB is R1, !.
fibonacci(N,FIB,R1,R2) :-
	N > 1,
	M is N-1,
	R1n is R1+R2,
	R2n is R1,
	fibonacci(M, FIB, R1n, R2n).

%exo 2.2
pgcd(A,A,X) :-
	X is A, !.
pgcd(A,B,X) :-
	A > B,
	An is A-B,
	pgcd(An,B,X), !.
pgcd(A,B,X) :-
	A < B,
	Bn is B-A,
	pgcd(A,Bn,X), !.

%exo 2.3
ackerman(0,N,X) :-
	X is N+1, !.
ackerman(M,0,X) :-
	M > 0,
	Mn is M-1,
	ackerman(Mn,1,X), !.
ackerman(M,N,X) :-
	M > 0,
	N > 0,
	Mn is M-1,
	Nn is N-1,
	ackerman(M,Nn,A),
	ackerman(Mn,A,X), !.
