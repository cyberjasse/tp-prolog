[1,2,3] = [1|X]. %retourne X = [2,3]
[1,2,3] = [1,2|X]. %devrait retourner X=[3]
[1 | [2,3]] = [1,2,X]. %devrait retourner X=3
[1 | [2,3,4]] = [1,2,X]. %retourne faux
[1 | [2,3,4]] = [1,2|X]. %devrait retourner X=[3,4]
b(o,n,j,o,u,r) = L. %retourne L = b(o,n,j,o,u,r). lol
bon(Y) =.. [X,jour]. %=.. operation de foncteur, transforme la liste en un predicat dont le premier element est le nom du predicat. Retourne Y=jour,X=bon
bon(Y) =.. [bon,jour]. %retourne Y=jour
X(Y) =.. [bon,jour]. %devrait retourner X=bon,Y=jour ? He non, retourne syntax error operator expected
[1 | [2 | [3]]] = L. %devrait retourner L = [1,2,3]
[1 | [2 | [3]]] = [X | Y]. %devrait retourner X=1 et Y=[2,3]
