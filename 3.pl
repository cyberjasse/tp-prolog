%exo 3.2
member2(E,[E|_]).
member2(E,[_|Y])
	member2(E,Y).

%exo 3.3
subset2([],_).
subset2([F|R],L2) :-
	member2(F,L2),
	subset2(R,L2).

%exo 3.4
takeout(E,[E|L3],L3) :-
takeout(E,[X|L3],[X|L4]):-
        takeout(E,L3,L4).

%exo 3.5
getEltFromList([X|_],N,E) :-
	N =:= 1,
	E = X.
getEltFromList([_|L2],N,E) :-
	N > 1,
	getEltFromList(L2,N-1,E).

%exo 3.6 terminale
inverse1(Li,Lf) :-
	inverse1(Li,Lf,[]).
inverse1([],Lf,T) :-
	Lf = T, !.
inverse1([X1|L1],Lf,T) :-
	inverse1(L1,Lf,[X1|T]).

%exo 3.7 v2
maxmin1([X|L],Max,Min) :-
	maxmin1(L,Max,Min,X,X), !.
maxmin1([],Max,Min,X1,X2) :-
	Max = X1,
	Min = X2, !.
maxmin1([X|L],Max,Min,X1,X2) :-
	X > X1,
	maxmin1(L,Max,Min,X,X2), !.
maxmin1([X|L],Max,Min,X1,X2) :-
	X < X2,
	maxmin1(L,Max,Min,X1,X), !.
maxmin1([_|L],Max,Min,X1,X2) :-
	maxmin1(L,Max,Min,X1,X2), !.

%exo 3.8
flatten([],[]) :-
	!.
flatten([X|L], Flatlist) :-
	is_list(X),
	flatten(X, Xf),
	flatten(L, Lf),
	append(Xf,Lf,Flatlist), !.
flatten([X|L], Flatlist) :-
	flatten(L,F),
	Flatlist = [X|F], !.
