%exo 4.2

preordre(tree(Noeud,G,D) , List) :-
	preordre(G,X1),
	preordre(D,X2),
	append([Noeud|X1],X2,List).
preordre(leaf(X),[X]).

inordre(tree(Noeud,G,D), List) :-
	inordre(G,X1),
	inordre(D,X2),
	append(X1,[Noeud|X2],List).
inordre(leaf(X),[X]).

postordre(tree(Noeud,G,D), List) :-
	postordre(G,X1),
	postordre(D,X2),
	append(X1,X2,X3),
	append(X3,[Noeud],List).
postordre(leaf(X),[X]).

%preordre( tree('*',tree('+',leaf(5),leaf(6)),tree('-',leaf(3),tree('/',leaf(2),leaf(2)))) ,L).
