%exo 5.1 pas dans l'ordre
union3(L1,L2,U) :-
	union3(L1,L2,U,[]).
union3([],[],U,U).
union3([X|L1], L2, U, Temp) :-
	member(X,Temp),
	union3(L2,L1,U,Temp), !.
union3(L1, [X|L2], U, Temp) :-
	member(X,Temp),
	union3(L2,L1,U,Temp), !.
union3([X|L1], L2, U, Temp) :-
	union3(L2,L1,U,[X|Temp]), !.
union3(L1, [X|L2], U, Temp) :-
	union3(L2,L1,U,[X|Temp]), !.

%exo 5.2 pas dans l'ordre
intersection3(L1,L2,I) :-
	intersection3(L1,L2,I,[]).
intersection3([], _, I, I).
intersection3([X|L1], L2, I, Temp) :-
	member(X,L2),
	intersection3(L1,L2,I,[X|Temp]), !.
intersection3([_|L1], L2, I, Temp) :-
	intersection3(L1,L2,I,Temp), !.
