%exo 6.1
map(_,[],[]).
map(P,[X|L1],[Y|L2]) :-
	call(P, X, Y),
	map(P, L1, L2).

%exo 6.2
filter(_,[],[]).
filter(F, [X|L1], [X|L2]) :-
	call(F, X), !,
	filter(F, L1, L2).
filter(F, [_|L1], L2) :-
	filter(F, L1, L2).

%exo 6.3
disjoint(L1,L2) :-
	forall(member(E,L1), not(member(E,L2))).

%exo 6.4
subset3(L1,L2) :-
	forall(member(E,L1), member(E,L2)).

%exo 6.5
intersection(L1,L2,R) :-
	findall(E, (member(E,L1),member(E,L2)), R).

%exo 6.6 du nouveau pdf d'enonce
preo(leaf(X), X).
preo(tree(X,_,_), X).
preo(tree(_,G,_), X) :-
	preo(G,X).
preo(tree(_,_,D), X) :-
	preo(D,X).
preordre(T, Res) :-
	findall(X,preo(T,X),Res).

ino(leaf(X), X).
ino(tree(_,G,_), X) :-
	ino(G,X).
ino(tree(X,_,_), X).
ino(tree(_,_,D), X) :-
	ino(D,X).
inordre(T, Res) :-
	findall(X, ino(T,X), Res).

posto(leaf(X), X).
posto(tree(_,G,_), X) :-
	posto(G,X).
posto(tree(_,_,D), X) :-
	posto(D,X).
posto(tree(X,_,_), X).
postordre(T, Res) :-
	findall(X, posto(T,X), Res).

%predicat pour tester
square(X,Y) :- Y is X*X.
positive(X) :- X>0.
%arbre pour exo 6.6
%preordre( tree('*',tree('+',leaf(5),leaf(6)),tree('-',leaf(3),tree('/',leaf(2),leaf(2)))) , L ).
