%exo 7.1
:- dynamic fibo/2.
fibo(1,1) :- !.
fibo(2,1) :- !.
fibo(X,_) :-
	X<1, !,
	fail.
fibo(N,F) :-
	N1 is N-1,
	N2 is N-2,
	fibo(N1, R1),
	fibo(N2, R2),
	F is R1+R2,
	asserta(fibo(N,F)), !.
%asserta ajoute au debut et assertz ajoute a la fin
