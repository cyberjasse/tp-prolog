:- use_module(library(clpfd)).

% exo 8.1
distributeur(T, P, E2, E1, C50, C20, C10, RE2, RE1, RC50, RC20, RC10) :-
	RE2 =< E2,
	RE1 =< E1,
	RC50 =< C50,
	RC20 =< C20,
	RC10 =< C10,
	200*RE2 + 100*RE1 + 50*RC50 + 20*RC20 + 10*RC10 #= P-T.
