 %exo 9.1 marche pas bien
cotoie(A,B) :-
	(B-A) = 1.
cotoie(A,B) :-
	(A-B) = 1.
yabon([ida:A, lucie:B, katie:C, suzie:D]) :-
	not(cotoie(B,A)),
	A < C,
	not(cotoie(B,D)).

 %data pour le 9.2
arc(a,b,1).
arc(b,f,2).
arc(a,f,4).
arc(b,e,4).
arc(f,e,1).
arc(f,a,3).

 %tous les chemins entre a et b si pas de cycle
chemin(A,A,Acc,Acc).
chemin(A,B, Acc, L) :-
	arc(A,C,_),
	not(member(C,Acc)),
	chemin(C,B, [C|Acc], L).
chemin(A,B,L) :-
	chemin(A,B,[A],L1),
	reverse(L1,L).

touschemins(A,B,L) :-
	findall(E, chemin(A,B,E), L).

 %plus court chemins entre a et b
len([], 0) :- !.
len([_], 0) :- !.
len([X|L], C) :-
	len(L, C1),
	L = [Y|_],
	arc(X,Y,C2),
	C = C1+C2, !.
	
minchs([X], C, X) :-
	len(X,C), !.
minchs([X|L], C, X) :-
	minchs(L, C1, _),
	len(X,C),
	C < C1, !.
minchs([_|L], C, X) :-
	minchs(L, C, X), !.
	
pluscourtchemin(A,B,C,L) :-
	touschemins(A,B,Chs),
	minchs(Chs, C, L).
	
