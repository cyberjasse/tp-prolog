% aout 2014
% question 3 bidi
split(L, 0, [], L) :- !.
split(L, N, L1, L2) :-
	not(var(L)),
	N < 0,
	split(L, 0, L1, L2), !.
split(L, N, L1, L2) :-
	not(var(L)),
	length(L, N2),
	N > N2,
	split(L, 0, L1, L2), !.
split([X|L], N, L1, L2) :-
	N2 is N-1,
	split(L, N2, L3, L2),
	L1 = [X|L3], !.

% question 4
% compress
compress([],[]) :- !.
compress([X|L1], L2) :-
	[Y|_] = L1,
	X = Y, !,
	compress(L1, L2).
compress([X|L1], [X|L2]) :-
	compress(L1, L2), !.

% repetition
repetition(L) :-
	compress(L, L2),
	length(L, N1),
	length(L2, N2),
	not(N1 = N2).
