% aout 2015
% question 4
enlever(E,L,R) :-
	var(E),
	member(E,L),
	enlever(E,L,R).
enlever(_,[],[]) :- !.
enlever(E,[V|L],R) :-
	not(var(E)),
	V = E,
	enlever(E, L, R), !.
enlever(E,[X|L],R) :-
	not(var(E)),
	not(E = X),
	enlever(E, L, R1),
	R = [X|R1], !.

% B
sublist(L,S) :-
	findall( A, (member(E,L), enlever(E,L,A)), S).

% C
membre(X,L) :-
	enlever(X,L,F),
	not(F = L).

% question 5
max0(X,Y,Result) :-
	X =< Y,
	Result = Y.
max0(X,Y,Result) :-
	X > Y,
	Result = X.
max1(X,Y,Y) :- X =< Y.
max1(X,Y,X) :- X > Y.
max2(X,Y,Y) :- X =< Y, !.
max2(X,Y,X) :- X > Y.
max3(X,Y,Y) :- X =< Y, !.
max3(X,Y,X).
max4(X,Y,Z) :-
	X =< Y, !,
	Y = Z.
max4(X,Y,X).
max5(X,Y,Z) :-
	X =< Y,
	Y = Z.
max5(X,Y,Z).

%max2 A=3. A=3. false. true.
%max3 A=3. A=3. true. true.
%max4 A=3. A=3. false; true. true.
%max5 A=3; true. true. true; true.

% question 6
child(jean, anne).
child(marie, steve).
child(marie, anne).
child(alice, anne).
child(alice, steve).
child(jean, steve).
child(jeanne, steve).
male(steve).
male(jean).
female(anne).
female(marie).
female(alice).
female(jeanne).

sister(S1 , S2) :-
	child(S1, P1), child(S1, P2),
	female(P1), male(P2),
	child(S2, P1), child(S2, P2),
	female(S1), female(S2),
	not(S1 = S2).
% A=marie B=alice; A=alice B=marie

sister2(S1 , S2) :-
	female(S1), female(S2),
	not(S1 = S2),
	child(S1, P1),
	female(P1),
	child(S2, P2),
	male(P2),
	child(S2, P1), child(S2, P2).
