move(A,B) :-
	write( move(A , B) ), nl.
hanoi(0,_,_,_):-!.
hanoi(N,A,B,C) :-
	Nm is N-1,
	hanoi(Nm,A,C,B),
	move(A,C),
	hanoi(Nm,B,A,C).
