% janvier 2014
% question 3
male(john).
male(jim).
male(mike).
female(mary).
female(lisa).
female(ann).
marriedTo(john,mary).
child(john,ann).
child(mary,ann).
child(john,jim).
child(mary,jim).
child(john,mike).
child(lisa,mike).

father(X,Y) :- child(Y,X), male(Y).
mother(X,Y) :- child(Y,X), female(Y).
halfBrother(A,B) :-
	male(B), father(A,P), mother(B,Q),
	not(A==B), not(marriedTo(P,Q)).
