% janvier 2015
% question 4
fold2(P, [X], I, R) :-
	call(P, X, I, R).
fold2(P, [X|L], I, R) :-
	fold2(P, L, I, R2),
	call(P, X, R2, R).

plus(A,B,C) :- C is A+B.
times(A,B,C) :- C is A*B.

%question 5
% Z=3 X=1 Y=[2,3]
% X=1 Y=[2,3]
% false
% X=point(1,2) Y=point(3,4), Z=T

%question 6
% A
numbers( X, [X] ) :-
	number(X).
numbers( binary(A,B,C) ,L) :-
	numbers(B, Lg),
	numbers(C, Ld),
	append(Lg, Ld, Temp),
	L = [A|Temp].
% a tester sur numbers(binary(8, binary(4,binary(2,1,3),binary(6,5,7)),binary(10,9,11)) , L).

% B
max(A,B,C) :- A=<B, C=B, !.
max(A,_,A).

depth( binary(_,B,C), N) :-
	depth(B, Ng),
	depth(C, Nd),
	max(Ng,Nd, T),
	N is T+1, !.
depth(_,0) :- !.
% a tester sur depth(binary(8, binary(4,binary(2,1,3),binary(6,5,7)),binary(10,9,11)) , L).

% C
left( binary(_,B,_), L) :-
	left(B,L), !.
left( L,L ):- !.

right( binary(_,_,C), R) :-
	right(C,R), !.
right( R,R ) :- !.

arb( binary(A,B,C) ) :-
	arb(B),
	arb(C),
	right(B,G),
	left(C,R),
	G < A,
	A < R, !.
arb(_) :- !.
% a tester sur arb(binary(8, binary(4,binary(2,1,3),binary(6,5,7)),binary(10,9,11))).

% D
find( binary(N,_,_) , N) :- !.
find( binary(_,B,_) , N) :- find(B,N), !.
find( binary(_,_,B) , N) :- find(B,N), !.
find( N, N) :- !.
