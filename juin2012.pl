% juin 2012
% question 4
drop(L1,N,L1) :-
	N < 1, !.
drop(L1,N,L1) :-
	length(L1, N2),
	N > N2, !.
drop(L1,N,L2) :-
	N2 is N-1,
	drop(L1,N2,L2,N2), !.

drop([],_,[],_) :- !.
drop([_|L1],N,L2,0) :-
	drop(L1,N,L2,N), !.
drop([X|L1],N,[X|L2],K) :-
	K2 is K-1,
	drop(L1,N,L2,K2), !.

% B
slice(L1,N1,_,[]) :-
	length(L1, B),
	not(between(1,B,N1)),!.
slice(L1,_,N2,[]) :-
	length(L1, B),
	not(between(1,B,N2)),!.
slice(_,N1,N2,[]) :-
	N2<N1,!.
slice([X|_],1,1,[X]) :- !.
slice([X|L1],1,N2,[X|L2]) :-
	N is N2 -1,
	slice(L1,1,N,L2),!.
slice([_|L1],N1,N2,L2) :-
	K1 is N1-1,
	K2 is N2-1,
	slice(L1,K1,K2,L2),!.

